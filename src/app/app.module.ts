import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableOrderModuleModule } from './table-order-module/table-order-module.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TableOrderModuleModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
