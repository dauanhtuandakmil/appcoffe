import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableOrderLayoutComponent } from './table-order-layout.component';

describe('TableOrderLayoutComponent', () => {
  let component: TableOrderLayoutComponent;
  let fixture: ComponentFixture<TableOrderLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableOrderLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableOrderLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
