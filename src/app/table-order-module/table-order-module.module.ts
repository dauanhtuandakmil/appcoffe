import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableOrderLayoutComponent } from './table-order-layout/table-order-layout.component';
import { TableHeaderComponent } from './table-header/table-header.component';
import { TableFooterComponent } from './table-footer/table-footer.component';
import { TableListComponent } from './table-list/table-list.component';
import { TableItemsComponent } from './table-items/table-items.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TableOrderLayoutComponent,
    TableHeaderComponent,
    TableFooterComponent,
    TableListComponent,
    TableItemsComponent
  ], exports: [
    TableOrderLayoutComponent,
  ]
})
export class TableOrderModuleModule { }
